import os, sys
feedfeed_path = os.path.abspath('../lib')
sys.path.append(feedfeed_path)
from feedfeed import feedfeed

from bs4 import BeautifulSoup
from textblob import TextBlob
import requests

feeds = os.path.abspath('./bitcoin_feeds.txt')

def get_bitcoin_price(currency='USD'):
    r = requests.get('https://api.bitcoinaverage.com/all')
    price = r.json()[currency]['averages']['ask']
    return str(price) + ' ' + currency

def article(entry, feed):

    content = None
    if ('content' in entry) and ('value' in entry.content):
        content = entry.content.value
    elif 'description' in entry:
        content = entry.description

    if content is not None:
        soup = BeautifulSoup(content)
        title = entry.title
        full_string = title + '. ' + soup.get_text()
        article = TextBlob(full_string)
        price = get_bitcoin_price()
        print title + ': ' + str(article.polarity) + ' with price at ' + price

feedfeed(filepath=feeds, callback=article, silent=True)
