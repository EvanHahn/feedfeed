import os, sys
feedfeed_path = os.path.abspath('../lib')
sys.path.append(feedfeed_path)
from feedfeed import feedfeed

feeds = os.path.abspath('./earthquake_feeds.txt')

def earthquake(entry):
    print 'New earthquake!'
    print entry.title

feedfeed(filepath=feeds, callback=earthquake)
