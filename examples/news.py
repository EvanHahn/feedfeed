import os, sys
feedfeed_path = os.path.abspath('../lib')
sys.path.append(feedfeed_path)
from feedfeed import feedfeed

feeds = os.path.abspath('./news_feeds.txt')

def article(entry, feed):
    print feed.title + ': ' + entry.title

feedfeed(filepath=feeds, callback=article)
