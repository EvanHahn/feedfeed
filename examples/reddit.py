import os, sys
feedfeed_path = os.path.abspath('../lib')
sys.path.append(feedfeed_path)
from feedfeed import feedfeed

feeds = os.path.abspath('./reddit_feeds.txt')

def post(entry, feed):
    print entry.title + ' on ' + entry.category

feedfeed(filepath=feeds, callback=post, silent=True)
