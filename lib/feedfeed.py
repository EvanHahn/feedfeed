import dateutil.parser as dateparser
import calendar
import time
import datetime
import feedparser
from array import array

def now():
    return datetime.datetime.now()

def published_to_long(string):
    parsed = dateparser.parse(string)
    return long(calendar.timegm(parsed.utctimetuple()))

def feedfeed(filepath, callback, delay=30, silent=False):

    if not silent:
        print 'Initializing feeds...'

    feed_count = 0
    for line in open(filepath):
        feed_count += 1

    last_seens = array('L')
    for i in xrange(feed_count):
        last_seens.append(0L)

    if not silent:
        print str(feed_count) + ' feeds found'

    pass_number = 0

    while True:

        start_time = now()
        pass_number += 1
        if not silent:
            print 'Pass ' + str(pass_number) + ' started at ' + str(start_time) + '...'
        index = 0
        for feed_url in open(filepath):
            last_seen = last_seens[index]
            feed = feedparser.parse(feed_url)
            for entry in feed.entries:
                publish_time = published_to_long(entry.published)
            if last_seens[index] == 0L:
                last_seen = publish_time
                last_seens[index] = publish_time
            elif publish_time > last_seens[index]:
                callback(entry, feed['feed'])
                last_seen = publish_time
            last_seens[index] = last_seen
            index += 1

        end_time = now()
        elapsed = (end_time - start_time).total_seconds()
        if elapsed < delay:
            wait_time = delay - elapsed
            if not silent:
                print 'Waiting ' + str(int(wait_time)) + ' seconds until next pass...'
            time.sleep(wait_time)
